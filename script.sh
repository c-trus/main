#!/bin/bash

# colors
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
white=`tput setaf 7`


# variables
template_path=$1
declare -A default_hash
declare -A yaml_content
save_file=save.yml
REGEX_YAML='(^[a-zA-Z].*)="(\[?"?[a-zA-Z].*|[0-9]*)"'


# functions
function parse_yaml {
  default_hash=()
  local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
  readarray -t lines < <(
    sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
    awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
        vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
        printf("%s%s=\"%s\"\n", vn, $2, $3);
      }
  }')
  for line in "${lines[@]}"; do
    [[ $line =~ $REGEX_YAML ]]
    key=${BASH_REMATCH[1]}
    value=${BASH_REMATCH[2]}
    default_hash[$key]=$value
  done
}

function command_exists {
  command -v "$1" >/dev/null 2>&1
}


# check if the user have already passed the tests
pass_tests=false
if [ -f "$save_file" ]; then
  parse_yaml_cmd="parse_yaml $save_file"
  $parse_yaml_cmd
  if [ ${default_hash[commands_available]} = true ]; then
    pass_tests=true
  fi
fi


# check if necessary commands are installed
if [ "$pass_tests" = false ]; then
  is_commands_missing=false
  docker_available=true
  if ! command_exists python; then
    if ! command_exists python3; then
      echo -e "${red}Command python not detected. You should install it here: https://docs.python-guide.org/starting/install3/linux"
      is_commands_missing=true
    fi
  fi
  if ! command_exists cookiecutter; then
    echo -e "${red}Command cookiecutter not detected. You should install it here: https://cookiecutter.readthedocs.io/en/latest/installation.html"
    is_commands_missing=true
  fi
  if ! command_exists git; then
    echo -e "${red}Command git not detected. You should install it here: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git"
    is_commands_missing=true
  fi
  if ! command_exists docker; then
    docker_available=false
    echo -e "${yellow}Command docker not detected. You should install it here: https://docs.docker.com/get-docker"
    if ! command_exists docker-compose; then
      echo -e "${yellow}Command docker-compose not detected. You should install it here: https://docs.docker.com/compose/install"
    fi
    echo -e "⚠️  ${yellow}You will not be able to start a dev environment with Docker."
  fi

  if $is_commands_missing; then
    exit
  else
    echo -n "" > $save_file
    echo -n "commands_available: true" >> $save_file
  fi
fi


# Read yaml file
if [ -z $template_path ]; then
  echo "Precise a template.yml file in argument"
  exit
fi
parse_yaml_cmd="parse_yaml $template_path"
$parse_yaml_cmd
for key in "${!default_hash[@]}"; do yaml_content[$key]=${default_hash[$key]}; done


# check if yaml is valid
mandatory_keys=('template_framework' 'template_project_name')
is_keys_missing=false
for key in $mandatory_keys; do
  if [ -z ${yaml_content[$key]} ]; then
    echo -e "${yellow}⚠️  Key «$key» not included"
    is_keys_missing=true
  fi
  if $is_keys_missing; then
    exit
  fi
done


# Select choosen template
framework=${yaml_content['template_framework']}
case $framework in
  nuxtjs)
    clone_cmd="git clone https://gitlab.com/c-trus/template_nuxtjs.git"
    template_name="template_nuxtjs"
    installation_instructions="npm install && npm run dev"
    ;;
  fastapi)
    clone_cmd="git clone https://gitlab.com/c-trus/template_fastapi.git"
    template_name="template_fastapi"
    installation_instructions="poetry install; uvicorn main:app --reload"
    ;;
  netcore)
    clone_cmd="git clone https://gitlab.com/c-trus/template_netcorewebapi.git"
    template_name="template_netcorewebapi"
    installation_instructions="TO DO"
    ;;
  symfony)
    clone_cmd="git clone https://gitlab.com/c-trus/template_symfony.git"
    template_name="template_symfony"
    installation_instructions="composer install; symfony server:start"
    ;;
  *)
    echo "${red}framework $framework not found"
    exit
    ;;
esac


# clone selected template
echo -e "${green}Running: $clone_cmd ${white}"
$clone_cmd


# edition cookiecutter.json
project_name=${yaml_content['template_project_name']}
project_slug=$project_name
project_slug="${project_slug,,}" # string to lowercase
if [ $framework = "netcore" ]; then
  project_slug=${project_slug//[ _-.&]/}
fi
project_slug=${project_slug//[ _]/-} # string to kebabcase
cd_cmd="cd $template_name"
$cd_cmd
filename="cookiecutter.json"
sed -i "s/\"project_slug\":.*\",/\"project_slug\": \"$project_slug\",/" $filename
sed -i "s/\"project_name\":.*\",/\"project_name\": \"$project_name\",/" $filename
cd_cmd="cd .."
$cd_cmd


# cookiecutter
cookie_cutter_cmd="cookiecutter $template_name"
echo -e "\n${green}Formatting template with your informations${white}"
$cookie_cutter_cmd


# delete old template
echo -e "\n${green}Cleaning..."
delete_old_template="rm -rf $template_name"
$delete_old_template


# create .git in project folder
if [ -d "$project_slug/" ]; then
  cd_cmd="cd $project_slug"
  git_init_cmd="git init"
  $cd_cmd
  $git_init_cmd
  ci_cd=${yaml_content['template_ci_cd']}
  if [ ! -z $ci_cd  ] && [ $ci_cd = 'true' ]; then
    read -p "${white}Complete the git url remote: " git_url
    git remote add origin $git_url
    git add .
    git commit -m "Initialize"
    git push --set-upstream origin master
    echo "${green}Your CI/CD is initialized."
  fi
fi


# copy .env.example to .env
if [ -f ".env.example" ]; then
  cp_cmd="cp .env.example .env"
  $cp_cmd
fi


# copy the correct docker-compose.yml file in the project
if [ $framework = "fastapi" ] || [ $framework = "symfony" ]; then
  parse_yaml_cmd="parse_yaml cookiecutter-config-file.yml"
  $parse_yaml_cmd
  database_type=${default_hash[default_context_database_type]}
  docker_compose_file="mysql" # default database type
  if [ database_type = "postgresql" ]; then
    docker_compose_file="postgresql"
  fi
  cp_cmd="cp docker/docker-compose.$database_type.yml docker-compose.yml"
  rm_cmd="rm -rf docker/"
  $cp_cmd
  $rm_cmd
fi


# messages
echo -e "\n${green}✅ Success !"
echo -e "${white}You can now run one of those commands:"
echo -e "➡️   cd $project_slug && docker-compose up\n"
echo -e "➡️   cd $project_slug && $installation_instructions"
